from datetime import datetime

import requests

s1 = datetime.now()
for i in range(0, 1000):
    url = "http://pft-analysis-services-development.eu-central-1.elasticbeanstalk.com/nlp"
    # url = "http://127.0.0.1:8000/nlp"

    payload = "{\"query\": \"une femme de 30 ans\", \"purpose\": \"searchProfile\", \"language\": \"fr\", \"context\": []}\n"
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache",
        'Postman-Token': "cd81594f-18c4-4e6c-9b86-18ffc3c91727"
    }

    r = requests.request("POST", url, data=payload, headers=headers)

    if r.status_code != 200:
        print('error in request # {}'.format(i))

    print(r.headers)
    exit(-1)

s2 = datetime.now()
print("total execution time: {}".format(s2 - s1))
